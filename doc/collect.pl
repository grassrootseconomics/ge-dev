#!/usr/bin/perl

use File::Basename;

my %prefix_translate = (
	'eth' => 'chainlib',
	'eth-token-index' => 'eth-address-index',
	'eth-address-declarator' => 'eth-address-index',
	'giftable-token' => 'giftable-erc20-token',
);

print qq{\@multitable {command} {module} {dependencies} {description}
};
my $srcdir = dirname(__FILE__) . '/items';
opendir (DIR, $srcdir);
while (my $d = readdir(DIR)) {
	if ($d =~ /^\./) {
		next;
	}
	$srcsubdir = $srcdir . '/' . $d;
	opendir (SUBDIR, $srcsubdir);
	while (my $dd = readdir(SUBDIR)) {
		if ($dd =~ /^\./) {
			next;
		}	
		open (FILE, $srcsubdir . '/' . $dd);
		my $description = <FILE>;
		chomp($description);
		my $dependencies = <FILE>;
		chomp($dependencies);
		my $module = $d;
#		if ($d =~ /^eth$/ ) {
#			$module = 'chainlib';
#		} elsif ($d eq 'eth-token-index' || $d eq 'eth-address-declarator') {
#			$module = 'eth-address-index';
#		}
		if ($prefix_translate{$d} ne '') {
			$module = $prefix_translate{$d};
		}
		my $cmd = $d . '-' . $dd;
		print qq{
\@item $cmd
\@tab $module
\@tab $dependencies
\@tab $description
};
	}
}
print qq{\@end multitable
};
