# How do I ....

Please make sure that you understand the [concepts](./CONCEPTS.md) before you proceed to this section.

These commands assume that settings for `CHAIN_SPEC`, `RPC_PROVIDER` and `WALLET_KEY_FILE` have been set appropriately. The values may be set/overridden for each tool using the `-i`, `-p` and `-y` flags respectively.

All of the following examples are specific to the EVM (Ethereum Virtual Machine). The term "token" refers to a token conforming to the ERC20 interface.

`<>` means "required value." `[]` means "optional value."


## Wallet commands


### Create a new wallet keyfile

`eth-keyfile`

In this case you will be prompted for a password, and the resulting key file will be output to standard output.

The `-z` flag can be provided to explicitly define a zero-length password:

`eth-keyfile -z > ./foo.json`


### Get the address of a wallet keyfile

`eth-keyfile -d <keyfile>`


### Get the private key of a wallet keyfile 

`eth-keyfile --private-key -d <keyfile>`


## Local commands


### Generate a checksum address

`eth-checksum <address>`

Example output:

```
$ eth-checksum eb3907ecad74a0013c259d5874ae7f22dcbcc95c
0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C
```

### Decode transactions

`echo <tx_data> | eth-decode`

Provides that the `tx_data` is signed with the same `chain_id` as the context.

A full round-trip example could be:

`eth-decode -i evm:foo:42 $(eth-gas -i evm:foo:42 -a <some_address> -y <key_file>)`

Example:

```
$ echo 0xf8652a843b9aca00825208940a11ce00000000000000000000000000000000008204008077a057504f183ee58acb58c4b71af937b7493d995234a3daa354481a929907db75dca07e73b5ef549baa2c90b6c3b33c0b4c1c96586bf1e172ba6380049641b481478f | eth-decode -i evm:foo:42
from: 0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C
to: 0x0A11CE0000000000000000000000000000000000
nonce: 42
gasPrice: 1000000000 (1 gwei)
gas: 21000
value: 1024 (0.000000000000001024 eth)
data: 0x
v: 119
recovery_byte: 0
r: 0x57504f183ee58acb58c4b71af937b7493d995234a3daa354481a929907db75dc
s: 0x7e73b5ef549baa2c90b6c3b33c0b4c1c96586bf1e172ba6380049641b481478f
chainId: 42
hash: 0x298009638c28134f238fc9bcd03ea7df2eb089cd8209d3aee1e3744822ab5a4b
hash_unsigned: 0x34c1d6a3a94cdc2fea499973bddc1eccf4e35d802f619595af2193f2db57d386
src: 0xf8652a843b9aca00825208940a11ce00000000000000000000000000000000008204008077a057504f183ee58acb58c4b71af937b7493d995234a3daa354481a929907db75dca07e73b5ef549baa2c90b6c3b33c0b4c1c96586bf1e172ba6380049641b481478f
```

## Network commands


### Get basic network stats

`eth-info`

Currently outputs:

* network id 
* block height
* fee price


### Get my network balance

`eth-balance <address>`


### Get details of a transaction known to the network

`eth-get <tx_hash>`


### Get the smart contract bytecode stored at an address

`eth-get <address>`

If no contract exists at the address, the output of this command will be empty.


### Send a network gas token transaction to someone

`eth-gas -a <recipient> -s <amount>`


### Format raw transaction data for RPC

`eth-raw <raw_tx>`

A practical example of which can be:

`eth-gas -a <some_address> 100 | eth-raw`

The result of this command can in turn be sent directly to a JSONRPC endpoint.

Example:

```
$ echo 0xf8652a843b9aca00825208940a11ce00000000000000000000000000000000008204008077a057504f183ee58acb58c4b71af937b7493d995234a3daa354481a929907db75dca07e73b5ef549baa2c90b6c3b33c0b4c1c96586bf1e172ba6380049641b481478f | eth-raw
{'jsonrpc': '2.0', 'id': '19a3cfae-5f72-4d37-8912-e5442a1d5ca2', 'method': 'eth_sendRawTransaction', 'params': ['0xf8652a843b9aca00825208940a11ce00000000000000000000000000000000008204008077a057504f183ee58acb58c4b71af937b7493d995234a3daa354481a929907db75dca07e73b5ef549baa2c90b6c3b33c0b4c1c96586bf1e172ba6380049641b481478f']}
```

## Token commands


### Check token balance of an address

`erc20-balance -e <token_address> <address>`


### Send tokens to an address

`erc20-transfer -e <token_address> -a <recipient_address> <token_amount>`

## CIC commands


### List all contracts in the contract registry

`eth-contract-registry-list -e <registry_address>`

Example output:

```
$ eth-contract-registry-list -e 0xea6225212005e86a4490018ded4bf37f3e772161
AccountRegistry	0xd0097a901AF4ac2E63A5b6E86be8Ad91f10b05d7
TokenRegistry	0xB6E6C1F1d62658164b7e6FF28D53a1095BEf1740
AddressDeclarator	0x32E860c2A0645d1B7B005273696905F5D6DC5D05
Faucet	0x103d1ed6e370dBa6267045c70d4999384c18a04A
TransferAuthorization	0xD18C525CC5e56e96998Dd596801B31a433E87708
ContractRegistry	0xEa6225212005E86a4490018Ded4bf37F3E772161
```

In the following the key name of the resource is presumed as the parameter:


#### List all known wallet accounts

`eth-accounts-index-list -e <AccountRegistry>`


#### List all known tokens

`eth-token-index-list -e <TokenRegistry>`


#### Get address from a token symbol

`eth-token-index-list -e <TokenRegistry> <token_symbol>`


#### Check if a particular address has used the faucet

`eth-faucet-list -e <Faucet> <address>`


#### Get a trusted proof for an address

`eth-address-declarator -e <AddressDeclarator> --declarator-address <trusted_signer_address> <address_to_check>`


## Common command modifiers


### Dump effective configuration

Chaintool uses the [`python-confini`](https://gitlab.com/nolash/python-confini) package to compile configurations.

It facilitates overriding all configuration directives with a collection of ini files, environment variables and command line arguments.

In order to debug what configuration settings in effect for a particular execution context, all tools offer the `--dumpconfig` switch to output the active configuration.

For example, `eth-balance` may output:

```
$ eth-balance --dumpconfig
RPC_HTTP_PROVIDER=http://localhost:8545
RPC_PROVIDER=http://localhost:8545
RPC_AUTH=
RPC_CREDENTIALS=
RPC_DIALECT=default
RPC_SCHEME=http
CHAIN_SPEC=evm:ethereum:1
WALLET_KEY_FILE=
WALLET_PASSPHRASE=
CONFIG_USER_NAMESPACE=
```

To make an environment override file, it can be useful to omit the values. This is done by adding the `--raw`  flag:

```
$ eth-balance --dumpconfig --raw
RPC_HTTP_PROVIDER=
RPC_PROVIDER=
RPC_AUTH=
RPC_CREDENTIALS=
RPC_DIALECT=
RPC_SCHEME=
CHAIN_SPEC=
WALLET_KEY_FILE=
WALLET_PASSPHRASE=
CONFIG_USER_NAMESPACE=
```


### Send transactions to RPC

By default, no signed transactions generated by any tool should be automatically sent to the network.

The tool can be instructed to send the transaction by specifying the `-s` flag.

#### Waiting for results

By default, the output from a tool that sends a transaction is the transaction hash.

The tool can instead be instructed to wait for the transaction to be included by the network, and return the _result_ of the transaction instead. To do this, specify the `-w` or `-ww` flag.

What exactly the "result" of a transaction will vary between tools. For example, a tool that _deploys_ a contract will should the _contract address_ as the "result."

Consult the tools' `--help` output for more details.


### Query past states

Use `--height` to query state at given block height instead of latest state

Imagine for the following example that address `0x0a11ce0000000000000000000000000000000000` received its first EVM gas token balance of `123.456000000000000000` (18 decimals) at block `42`:

```
$ eth-balance 0x0a11ce0000000000000000000000000000000000
123.456000000000000000
$ eth-balance --height 41 0x0a11ce0000000000000000000000000000000000
0.000000000000000000
```


### Create output useful for machines

Use the option `--raw` to generate minimal and/or output intended for machines.

The format of the output depends on the tool.

Imagine for the following example that address `0x0a11ce0000000000000000000000000000000000` has an EVM gas token balance of `123.456000000000000000` (18 decimals):

```
$ eth-balance 0x0a11ce0000000000000000000000000000000000
123.456000000000000000
$ eth-balance --raw 0x0a11ce0000000000000000000000000000000000
123456000000000000000
```


