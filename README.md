# cli tools


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>

<meta name="description" content="Untitled Document">
<meta name="keywords" content="Untitled Document">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<table>
<tr><td>cic-eth-ctl</td><td>cic-eth</td><td>cic-eth-taskerd</td><td>Queue control</td></tr>
<tr><td>cic-eth-create</td><td>cic-eth</td><td>cic-eth-taskerd</td><td>Custodial account creation</td></tr>
<tr><td>cic-eth-tag</td><td>cic-eth</td><td>db (must be refactored)</td><td>Assign custodial account to system role</td></tr>
<tr><td>cic-eth-inspect</td><td>cic-eth</td><td>cic-eth-taskerd</td><td>Query queue txs and states</td></tr>
<tr><td>cic-eth-resend</td><td>cic-eth</td><td>cic-eth-taskerd</td><td>Force resend of queue tx</td></tr>
<tr><td>eth-contract-registry-list</td><td>eth-contract-registry</td><td>evm</td><td>Dump identifier address pairs</td></tr>
<tr><td>eth-contract-registry-seal</td><td>eth-contract-registry</td><td>evm</td><td>Seal registry contract</td></tr>
<tr><td>eth-contract-registry-set</td><td>eth-contract-registry</td><td>evm</td><td>Assign identifier to address</td></tr>
<tr><td>eth-contract-registry-deploy</td><td>eth-contract-registry</td><td>evm</td><td>Deploy registry to evm</td></tr>
<tr><td>eth-decode</td><td>chainlib</td><td>-</td><td>Displays a deserialized signed RLP transaction</td></tr>
<tr><td>eth-gas</td><td>chainlib</td><td>evm</td><td>Send network token amount</td></tr>
<tr><td>eth-balance</td><td>chainlib</td><td>evm</td><td>Current network token or network token balance for an address</td></tr>
<tr><td>eth-checksum</td><td>chainlib</td><td>-</td><td>Convert address to checksum address</td></tr>
<tr><td>eth-transfer</td><td>chainlib</td><td>evm</td><td>ERC20 token transfer</td></tr>
<tr><td>eth-keyfile</td><td>crypto-dev-signer</td><td>-</td><td>Create and unlock ethereum wallet keystore files</td></tr>
<tr><td>eth-get</td><td>chainlib</td><td>evm</td><td>Current network status of tx</td></tr>
<tr><td>confini-dump</td><td>confini</td><td>(none)</td><td>Print all configuration entries as envirionment pairs</td></tr>
<tr><td>eth-address-declarator-add</td><td>eth-address-index</td><td>evm</td><td>Add an address declaration</td></tr>
<tr><td>eth-address-declarator-deploy</td><td>eth-address-index</td><td>evm</td><td>Deploy AddressDeclarator contract</td></tr>
<tr><td>sarafu-token-deploy</td><td>sarafu-token</td><td>evm</td><td>Deploy Sarafu Token contract</td></tr>
<tr><td>cic-ussd-client</td><td>cic-ussd</td><td>cic-ussd-server</td><td>Terminal emulator for USSD session</td></tr>
<tr><td>giftable-token-gift</td><td>giftable-erc20-token</td><td>evm</td><td>Mint tokens to address</td></tr>
<tr><td>giftable-token-add</td><td>giftable-erc20-token</td><td>evm</td><td>Add account allowed to mint tokens</td></tr>
<tr><td>giftable-token-deploy</td><td>giftable-erc20-token</td><td>evm</td><td>Deploy Giftable Token contract</td></tr>
<tr><td>eth-token-index-list</td><td>eth-address-index</td><td>evm</td><td>List all tokens in index</td></tr>
<tr><td>eth-token-index-add</td><td>eth-address-index</td><td>evm</td><td>Add token to token index</td></tr>
<tr><td>eth-token-index-deploy</td><td>eth-address-index</td><td>evm</td><td>Deploy token index contract</td></tr>
<tr><td>sarafu-faucet-gift</td><td>sarafu-faucet</td><td>evm</td><td>Use faucet for address</td></tr>
<tr><td>sarafu-faucet-set</td><td>sarafu-faucet</td><td>evm</td><td>Set new faucet amount</td></tr>
<tr><td>sarafu-faucet-deploy</td><td>sarafu-faucet</td><td>evm</td><td>Deploy Sarafu Faucet contract</td></tr>
<tr><td>erc20-single-shot-faucet-list</td><td>erc20-single-shot-faucet</td><td>evm</td><td>Check faucet status for an address</td></tr>
</table>



</body>
</html>
