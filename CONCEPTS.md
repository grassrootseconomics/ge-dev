# An introduction to CIC development tools

This is a primer describing the main concepts behind the CIC (Community Inclusion Currency) CLI (Command Line Interface) tools, aswell as how to use them.

Since the first implementation of CIC software is centered around the EVM (Ethereum Virtual Machine) technology, EVM-specific concepts will be used in cases where clarity requires concrete examples.

However, it's important to keep in mind that the CIC software project is designed around concepts and interfaces that are believed to be universally valid for all blockchain technologies.

If you know the basics are are merely looking for quick recipes to do specific stuff with tools, then consult  [HOWTO_ETH.md](./HOWTO_ETH.md) instead.


## Versions

This document was last updated for the following CIC python package version targets:

- `chainlib 0.0.9`
- `chainlib-eth 0.0.9`
- `crypto-dev-signer 0.4.15`
- `chaind 0.0.3`
- `chaind-eth 0.0.3`


## Common blockchain tooling concepts

Some concepts are common for nearly all services and tools built using the CIC software. It is useful to learn these by heart, aswell as how to commonly manipulate them.

Each of the concepts are accompanied with an argument flag used to set these values across all tools.

If an environment variable can be used to set the value, it is included too.

All of the properties described below should be recognizable when providing the `--help` flag to any CIC CLI tool. Notice that some tools that only _read_ state and do not _create state change transactions_ may have fewer argument flags available.


### Chain Spec

This is a 3-4 part value defining the `blockchain type`, `network type`, `network_id` and optionally `network label` of the network being operated on.

When passed as a string argument to a CLI, these values are separated by colons. This means, for example, that the [Bloxberg](https://bloxberg.org) EVM network, the soon-to-be home of the first CIC deployment, would be represented as `evm:byzantium:8995:bloxberg`

The `network_id` part the Chain Spec is especially important, because it is used as a parameter when signing and decoding transactions.

* arg: `-i`
* env: `CHAIN_SPEC`


### RPC Provider

An RPC endpoint through which the installed software knows how to interact with the blockchain node.

In the case of EVM this is a blockchain node offering an JSONRPC HTTP interface.

* arg: `-p`
* env: `RPC_PROVIDER`


### Keyfile

A file format used to store a (hopefully encrypted) private key, and what steps to take to load the key from the file.

A typical keystore file in the context of Ethereum is a `json` file, containing, in short:

- the address of the key it contains
- the encrypted key
- details on how to decrypt the key

It will look something like this:

```
{
  "address": "eb3907ecad74a0013c259d5874ae7f22dcbcc95c",
  "crypto": {
    "cipher": "aes-128-ctr",
    "ciphertext": "b0f70a8af4071faff2267374e2423cbc7a71012096fd2215866d8de7445cc215",
    "cipherparams": {
      "iv": "9ac89383a7793226446dcb7e1b45cdf3"
    },
    "kdf": "scrypt",
    "kdfparams": {
      "dklen": 32,
      "n": 262144,
      "p": 1,
      "r": 8,
      "salt": "299f7b5df1d08a0a7b7f9c9eb44fe4798683b78da3513fcf9603fd913ab3336f"
    },
    "mac": "6f4ed36c11345a9a48353cd2f93f1f92958c96df15f3112a192bc994250e8d03"
  },
  "id": "61a9dd88-24a9-495c-9a51-152bd1bfaa5b",
  "version": 3
}
```


* arg: `-y`
* env: `WALLET_KEY_FILE` (and `WALLET_PASSPHRASE` if protected by a password)


### Fee 

Getting a transaction executed on a blockchain network is actually a never-ending bidding war. Theoretically, whoever bids the most, gets their transaction executed first.

A fee can be understood either as a single value, or a price per unit of code that gets executed as a result of the transaction.

In the example of EVM, the "fee" is called "gas." There is a "gas limit" which roughly translates to a limit on how many code instruction units you want to have executed. There is also a "gas price" per instruction unit. The product of the two defines the maximum gas token units you are willing to spend for the transaction to go through. In this case, the "gas price" is the effective bid.

Different blockchains will surely have different dynamics here. Some may care only about the total ("fee limit" is 1), where others may prefer code unit allowances that are small, large, or at a certain multiplier, whatever maximizes their profit potential.

The intention is that if fee limit and/or fee price is _not_ specified, the RPC middleware should automatically determine the correct values.

* arg: `--fee-limit` and `--fee-price`


### Nonce

A blockchain may or may not enforce a [nonce](https://en.wiktionary.org/wiki/nonce) value per transaction. EVM does this.

The nonce value, at a minimum, will create different transaction hashes for transactions that are otherwise identical ("Alice sends 10 ETH to Bob"). They can be instrumental in preventing (accidental) double spends in the network.

As with the fee parameters above, if nonce is not specified, the RPC middleware should automatically determine the correct value.

* arg: `--nonce`


### Block height

By default, all state queries are performed against the last state the network has reached a consensus on.

A blockchain RPC should also make it possible to query the state at a _certain point in the past_. "Time" here translates to "block height," which means the state after a given block number in the chain was included.

* arg: `--height`


### Transactions

#### Sending to network

By default, the CIC tools should _not_ send transactions to the network. Instead, the output of the tool should be a serialized, signed transaction that may be independently submitted to the network.

* arg: `-s` to automatically send the transaction to the network (requires the RPC provider to be set). 


#### Waiting for results

Some tools perform operations that merely read the state of the blockchain. That is, the state according to the RPC node you are connected to, which may or may not be in sync with the last "global" state. Beware!

Other tools send transactions to the network in order to _change the state_ of the blockchain.

The result of a transaction isn't known before the transaction is included in a block that the network agrees belongs in the chain.

CIC tools include an option to _wait_ for and return the result of a transaction. Otherwise, the transaction hash will be returned, which can be used to query the transaction independently.

* arg: two options:
	1. `-w` to wait for the _last_ transaction executed by the tool. This typically yield the end result, disregarding other possible side-effects that the tool has caused.
	2. `-ww` to wait for _all_ transactions executed by the tool. When given this argument, the tool should make sure that all state changes it has caused are completed before returning control to the caller.



### Addressing

Any blockchain RPC worth anything will let you send a transaction to any address within its address space, regardless of whether someone has the key to that address or not.


#### Executable addresses

Some blockchains may treat certain address in a special manner. For example, an address may represent a pointer to some executable code, that will accept input from other transactions and _compute_ and _store_ a result based on the code stored behind that pointer.

In the case of EVM, these are called "contract addresses," which in turn refers to the term Smart Contract. This translates to a decentralized piece of code that may modify the state of a blockchain.

The CIC tooling generalizes this term to "executable addresses."

* arg: `-e` to set the _executable address_ of the transaction.


#### Recipient addresses

The CIC tooling considers the term "recipient address" to be the ultimate target of a transaction.

This means that while the actual "to-address" of a network transaction may be an executable address as described above, the _effect_ of executing that transaction may be that the token balance of the _recipient address_ and/or _sender address_ is adjusted.

In the case of EVM, the _executable address_ would be the token _smart contract address_, and the _recipient address_ would be e.g. the benefactor of a `transfer` call on that _executable address_.


* arg: `-a` to set the _recipient address_ of the _effect_ of the transaction.


### Checksums

To protect from typos in recipient addresses and resulting loss of funds, checksums may be implemented for addresses.

EVM implements a checksum implemented in the hex value of the address. The CIC tools should reject addresses that are not checksummed, unless explcitly told not to.

* arg: `-u` to explicitly ignore checksums


### Verbosity

The CIC tools should be very generous with logging output to assist in transparency and understanding of what's going on under the hood.

Generally, there are three levels of verbosity that should be made available through the CIC tools.

1. The "normal" level, in which any logging output received indicates a condition that _most likely should not have happened_. This translates to the "warning" debug level in geek terms.
1. The "info" level, in which significant state change events are included.
1. The "debug" level, in which nearly everything that's going on will be described. This will produce a **lot** of output.

* arg: `-v` for "info", `-vv` for "debug".


### Pipelines

Unless there are very good reasons not to, all CIC tools should enable single-value output and a single positional input, so that the output from one command can be easily provided as the input to another.

That means that any CIC tools should allow for input to be provided _either_ as a positional argument _or_ an argument provided through standard input.

The source of the input should be automatically detected. That is; an explicit argument should _not_ be required to determine whether or not to read from standard input.



## Common CIC concepts

In short, CIC is a way of thinking about what units of currency are (tokens), and how to organize a way of **discovering**, **authenticating** and **interacting** with them that is **democratic**, **predictable** and **conceptually coherent** across blockchain implementations.


### Contract registry

The collection of resources that belongs to a specific blockchain implementation of a CIC is in itself a hierarchical structure.

Conceptually, this means there is a single point on any blockchain from which all CIC resources can be discovered. This point functions as the "start of authority."

Resources can only be discovered if the registry address is explicitly provided. The CIC infrastructure is _not_ concerned with how a registry address for a particular chain is disseminated. (That does not mean that this is not a trivial problem, however.)

The contract registry interface is a simple key-value store that maps strings to network (executable) addresses.

Headlines in the sections that follow equal the key names in the registry:


#### `TokenRegistry`

A key/value store providing unique token symbol to executable address mappings.

This registry is not necessarily intended to be an endorsement for the token itself, but merely a disambiguation so that a client inadvertently interacts with a different token using the same symbol.


#### `AccountRegistry`

An array of addresses "known" to the particular CIC implementation on the network.


#### `AddressDeclarator`

Maps addresses to an arbitrary number of (256-bit) content-addresses, grouped by the addresses of the keys who added them to the state.

If you know the signer you trust, you receive an array of hashes when asking the signer's "opinion" of a particular address. These hashes can both be used with a compatible service to retreive the content represented by the hash, and to verify the content's integrity recalculating the hash based on the response.

See also "Permissionless trust building" below.


#### `Faucet`
An executable address that enables some value disbursement depending on the executors relation to the network.

Can for example enable a use-case as the following: Anyone added to the `AccountRegistry` receives a first-time disbursement of some token amount.


#### `ContractRegistry`

The address of the contract registry itself (loopback).


#### `TransferAuthorization`

Multi-signature mechanism that creates an allowance for a token value for a particular address, and enables a quorom of other addresses to perform a transaction on that allowance.


### Permissionless trust building

In a CIC network, anyone should be free to choose who to trust for information and endorsements.

In a CIC network, anyone should also be free to provide information and endorsements.

Let's say someone want to learn something about what a specific network address represents.

If that address is a token, you may want to know what is actually backing that token. To know this, you will want to learn what someone you trust have to say about that token, and any proofs that may have been provided to reinforce that claim.

Or perhaps the address is a wallet of a human currency user. In that case, one may want to know whether the identity of that individual has been confirmed.

At the heart this dynamic is simply the action of different parties signing one or more proofs, and the enquiring party selecting one or more parties to accept proofs from.

#### A somewhat contrived example

- Alice is part of a community that have started a currency `FOO`.
- Everyone in Alice's community signed a document describing what the currency respresents.
- Alice has a PDF of this document.
- Alice has pictures of each community member holding a sign with the hash of the PDF document.
- Alice signs the PDF hash and posts it to the `AddressDeclarator` contract.
- Alice signs each picture hash, and posts each to the AddressDeclarator contract.
- Bob wonders what `FOO` is. 
- Bob trusts Alice.
- Bob looks up which proof exists for `FOO` in the `AddressDeclarator` contract that's signed by Alice.
- Bob records a video of himself saying why he trusts `FOO`.
- Bob signs the hash of the video, and posts it to the `AddressDeclarator` contract.
- Carol trusts Bob.
- etc...
